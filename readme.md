# Introduction
Oculus, an image resizer which caches resized images. 

# Installation
1. Copy/paste require definition for Queue to your composer.json.

```
"require": {
    "codeyellow/queue": "*"
}
```

2. Add to app/config/config.php.

```
'always_load'  => array(
		'modules'  => array(
			'oculus' => APPPATH . 'vendor' . DS . 'codeyellow' . DS . 'fuelphp-oculus' . DS
		)
	)
```	
3. If you upgrade from a previous version, first migrate the fuelphp-queue migrations down. 

4. Perform migration.

5. Make files folder writable. Default is in fuel/app/vendors/codeyellow/fuelphp-oculus/files.