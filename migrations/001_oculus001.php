<?php
namespace Fuel\Migrations;

class Oculus001
{

    function up()
    {
        \DBUtil::create_table('oculi', array(
            'id' => array(
                'type' => 'int',
                'constraint' => 10,
                'auto_increment' => true,
                'unsigned' => true
            ),
            'name' => array(
                'type' => 'varchar',
                'constraint' => 255 
            ),
            'name_unpurified' => array(
                'type' => 'varchar',
                'constraint' => 255 
            ),
            'type' => array(
                'type' => 'varchar',
                'constraint' => 31
            ),
            'mimetype' => array(
                'type' => 'varchar',
                'constraint' => 31
            ),
            'size' => array(
                'type' => 'int',
                'constraint' => 15
            ),
            'saved_as' => array(
                'type' => 'varchar',
                'constraint' => 255
            ),
            'created_at' => array(
                'type' => 'int',
                'constraint' => 11 
            ),
            'updated_at' => array(
                'type' => 'int',
                'constraint' => 11 
            ) 
        ), array(
            'id' 
        ));
    }

    function down()
    {
        \DBUtil::drop_table('oculi');
    }
}