<?php
namespace Oculus;

/**
 * Image Not Found Exception
 */
class ImageNotFound extends \OutOfBoundsException
{
}

/**
 * Oculus, an image resizer which caches resized images. 
 * 
 * [Features] 
 * 
 * - Resize images using image path arguments such as 'w/100/h/100.jpg'. 
 * - Cache resized images. 
 * - Bypass Oculus when there is a cached image. 
 * 
 * 
 * [Changelog] 
 * 
 * [1.1.0] 
 * - Bypasses Oculus completely if there is a cached image. 
 * - Added exceptions. 
 * - Added config 'error_image_not_found', 'error_invalid_argument', 'error_unknown'.
 * 
 * [1.1.1]
 * - Added (dependency) Queue package.
 * 
 *
 * [Roadmap]
 * 
 * - Refactoring.
 * - Driver based model.
 * - Secondary server for image proccessing.
 * - Push notification after image processing done.
 * 
 * @author AB Zainuddin
 * @namespace Oculus
 * @version 1.1.1
 * @copyright Code Yellow B.V.
 */
class Model_Oculus extends \Crux\Model
{
    const CONFIG_NAME = 'Oculus';
    protected static $_table_name = 'oculi';
    private $config = array();

    function __construct(array $data = array(), $new = true, $view = null)
    {
        parent::__construct($data, $new, $view);
        
        static::init();
    }

    static public function addImages(array $images)
    {
        $result = array();
        
        foreach ($images as $index => $imageAttributes) {
            $result[] = static::addImage($imageAttributes);
        }
        
        return $result;
    }

    static public function addImage(array $props)
    {
        $model = static::forge($props);
        $model->save();
        
        return $model;
    }

    /**
     * Load config file.
     */
    static public function init()
    {
        \Config::load(__DIR__ . DS . '..' . DS . '..' . DS . 'config' . DS . 'oculus-default.php', 'oculus');
        \Config::load('oculus', 'oculus');
    }

    /**
     * Get image.
     *
     * @param array $args
     */
    static public function getImage($args)
    {
        static::init();
        
        $args = static::_extract_args($args);
        $cachepath = static::_get_cache_path($args);
        
        if (!file_exists($cache)) {
            static::process($args);
        }
        
        static::_output($cache);
    }

    /**
     * Get image by id.
     *
     * @param array $originalArgs
     */
    static public function getImageById($originalArgs)
    {
        static::init();
        
        // Process shorthand codes.
        $processedArgs = static::aliases($originalArgs);
        
        // Get filename from database where file is id of model.
        if (isset($processedArgs['file']) && $model = static::find($processedArgs['file'])) {
            $processedArgs['file'] = static::_get_image_path($model->saved_as);
        }

        // Check if image exists, render image not found.
        if (!static::_image_exists($processedArgs['file'])) {
            $processedArgs['file'] = \Config::get('oculus.error_image_not_found');
        }
        
        // Process args.
        try {
            $cachepath = static::_get_cache_path($originalArgs);
            $checkedArgs = static::checkArgs($processedArgs);
            
            static::queue($checkedArgs, $cachepath, $originalArgs);
            static::_output($cachepath);
        } catch (ImageNotFound $e) {
            static::_output(\Config::get('oculus.error_image_not_found'));
        } catch (\InvalidArgumentException $e) {
            static::_output(\Config::get('oculus.error_invalid_argument'));
        } catch (\Exception $e) {
            static::_output(\Config::get('oculus.error_unknown'));
        }
    }

    static function flush_cache()
    {
        static::init();
        
        $dir = \File::delete_dir(static::_get_cache_path(), true, false);
        
        // TODO: bug report?
        // Some error occurs if deleting dir.
        // $dir->delete_dir(true, false);
        // \Debug::dump($dir);die();
        // $files = $dir->delete();
        
        // foreach($files as $file)
        // {
        
        // $file->delete();
        
        // }
    }

    /**
     * Start output of image.
     *
     * @param string $file_path
     */
    static private function _output($file_path)
    {
        // $uri = \Config::get('oculus.uri_cache') . pathinfo($file_path,
        // PATHINFO_BASENAME);
        // \Debug::d($uri);
        // \Response::redirect($uri, 'location', 301);
        \Image::load($file_path)->output();
    }

    public static function aliases($args)
    {
        $result = array();
        $keys = \Config::get('oculus.arg_keys');
        
        foreach ($keys as $key => $aliases) {
            if (!is_array($aliases)) {
                $key = $aliases;
                $aliases = array(
                    $aliases 
                );
            } else {
                $aliases[] = $key;
            }
            
            // Loop each $alias hoping to match 1.
            foreach ($aliases as $alias) {
                if (isset($args[$alias])) {
                    $result[$key] = $args[$alias];
                }
            }
        }
        
        return $result;
    }

    public static function checkArgs($args)
    {
        // Check file.
        if (!isset($args['file']) || strlen($args['file']) <= 0 || !static::_image_exists($args['file'])) {
            throw new ImageNotFound();
        }
        
        // Check max width.
        if (isset($args['width'])) {
            $args['width'] = (int)$args['width'] > 0 ? (int)$args['width'] : 0;
            
            if ($args['width'] > \Config::get('oculus.max_width')) {
                $args['width'] = \Config::get('oculus.max_width');
            }
        }
        
        // Check max height.
        if (isset($args['height'])) {
            $args['height'] = (int)$args['height'] > 0 ? (int)$args['height'] : 0;
            
            if ($args['height'] > \Config::get('oculus.max_height')) {
                $args['height'] = \Config::get('oculus.max_height');
            }
        }
        
        // Check aspect ratio.
        if (isset($args['aspect_ratio'])) {
            $args['aspect_ratio'] = (bool)$args['aspect_ratio'];
        }
        
        // Check padding.
        if (isset($args['padding'])) {
            $args['padding'] = (bool)$args['padding'];
        }
        
        // Check padding bgcolor.
        if (isset($args['padding_bgcolor'])) {
            $args['padding_bgcolor'] = '#' . $args['padding_bgcolor'];
        }
        
        return $args;
    }

    /**
     * Extract args.
     *
     * @param array $args
     */
    static private function _extract_args($args)
    {
        return static::checkArgs(static::aliases($args));
    }

    static public function queue(array $args, $cachePath, $originalArgs)
    {
        $img = \Image::load(\Config::get('oculus.processing'));
        
        // Create dir.
        if (!file_exists(pathinfo($cachePath, PATHINFO_DIRNAME))) {
            \File::create_dir(static::_get_cache_path(), substr(pathinfo($cachePath, PATHINFO_DIRNAME), strlen(static::_get_cache_path())));
        }
        
        // Save image.
        $img->save($cachePath, 0766);
        
        // Process args.
        if (\Arr::get($args, 'width') || \Arr::get($args, 'height', null)) {
            $img->config('quality', \Arr::get($args, 'quality', 100));
            $img->config('bgcolor', \Arr::get($args, 'padding_bgcolor', null))->resize(\Arr::get($args, 'width', null), \Arr::get($args, 'height', null), \Arr::get($args, 'aspect_ratio', true), \Arr::get($args, 'padding', false));
        }
        

        // Create a job in the queue.
        new \CodeYellow\Queue\Job(
            'Oculus\Model_Oculus',
            'process',
            array(
                'ar' => $args,
                'cachePath' => $cachePath,
                'originalArgs' => $originalArgs
            ),
            'oculus'
        );
    }

    public static function process_queue()
    {
        $processes = \DB::select('*')->from('oculus_processes')->where('is_started', 0)->execute();
        
        // Only if there is a queue.
        if (count($processes) > 0) {
            if (\DB::update('oculus_queues')->value('is_started', 1)->where('is_started', 0)->execute()) {
                // Get first not yet started.
                $process = $processes->current();
                
                // Process.
                \DB::update('oculus_processes')->value('is_started', 1)->where('id', $process['id']);
                $command = json_decode($process['command']);
                static::process((array)$command->args, $command->cachePath);
                
                // Update.
                \DB::update('oculus_processes')->value('is_finised', 1)->where('id', $process['id']);
                \DB::update('oculus_queues')->value('is_started', 0);
            }
        }
    }

    /**
     * Process commands in $args and save cache file.
     *
     * @param object $args
     */
    static public function process($args, $cachePath)
    {
        $args = (array) $args;
        static::init();
        ini_set('memory_limit', '1024M');
        
        $img = \Image::load($args['file']);

        // Process args.
        if (\Arr::get($args, 'width') || \Arr::get($args, 'height', null)) {
            $img->config('bgcolor', \Arr::get($args, 'padding_bgcolor', null));

            switch(true) {
                case \Arr::get($args, 'crop_resize'):
                    $img->crop_resize(\Arr::get($args, 'width', null), \Arr::get($args, 'height', null));
                    break;
                default:
                    $img->resize(\Arr::get($args, 'width', null), \Arr::get($args, 'height', null), \Arr::get($args, 'aspect_ratio', true), \Arr::get($args, 'padding', false));
                    break;
            }
        }
        
        // Create dir.
        // if (!file_exists(pathinfo($cachePath, PATHINFO_DIRNAME))) {
        // \File::create_dir(static::_get_cache_path(),
        // substr(pathinfo($cachePath, PATHINFO_DIRNAME),
        // strlen(static::_get_cache_path())));
        // }
        
        // Save image.
        $img->save($cachePath);
    }

    /**
     * Get image path from $file_name.
     *
     * @param string $file_name
     */
    static private function _get_image_path($file_name)
    {
        return \Config::get('oculus.path_files') . $file_name;
    }

    /**
     * Check if image exists.
     *
     * @param string $file_name
     */
    static private function _image_exists($file_name)
    {
        return file_exists($file_name);
    }

    /**
     * Get cache file from $args or from config if $args is empty.
     *
     * @param array $args
     * @return string
     */
    static private function _get_cache_path(array $args = array())
    {
        if ($args) {
            return \Config::get('oculus.path_cache') . static::_get_cache_name($args);
        } else {
            return \Config::get('oculus.path_cache');
        }
    }

    /**
     * Resolve cache name.
     *
     * @param array $args
     * @return string
     */
    static private function _get_cache_name(array $args)
    {
        $path = '';
        $lastKey = \Arr::lastKey($args);
        $lastVal = \Arr::last($args);
        
        unset($args[$lastKey]);
        
        $path = \Arr::implode_assoc($args, DS, DS) . DS . $lastKey . DS . $lastVal;
        
        if (strlen(pathinfo($path, PATHINFO_EXTENSION)) <= 0) {
            throw new \InvalidArgumentException('Missing image extension');
        }
        
        return $path;
    }

    /**
     * Encode cache file name.
     *
     * @param string $file_name
     * @return string
     */
    static private function _encode_cache_file_name($file_name)
    {
        return strtr(base64_encode($file_name), '+/=', '-_,');
    }

    /**
     * Decode cache file name.
     *
     * @param string $file_name
     * @return string
     */
    static private function _decode_cache_file_name($file_name)
    {
        return base64_decode(strtr($file_name, '-_,', '+/='));
    }
}