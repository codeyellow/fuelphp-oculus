<?php
namespace Oculus;
/**
 * The Update Controller.
 *
 * Update module Oculus.
 * 
 * @package  Oculus
 * @extends  Controller
 */
class Controller_Update extends \Controller
{
    /**
     * Process update.
     */
	public function action_index()
	{
		\Migrate::latest('oculus', 'module');
	}
}
