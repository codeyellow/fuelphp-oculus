<?php
namespace Oculus;
/**
 * The Upload Controller.
 * 
 * @package  Oculus
 * @extends  Controller
 */
class Controller_Upload extends \Controller_Rest
{
    /**
     * Upload files.
     */
	public function action_index()
	{
	    $images = array();
		$success = true;
		Model_Oculus::init();
		
		$config = array(
		    'path' => \Config::get('oculus.path_files'),
		    'randomize' => true,
		);
		
		// process the uploaded files in $_FILES
		try{
			\Upload::process($config);

			if(\Upload::is_valid()){
		    	\Upload::save();
		    	$this->response(Model_Oculus::addImages(\Upload::get_files()), \Response::status('OK'));
			} else {
				throw new \InvalidArgumentsException();
			}
		} catch (\FuelException $e) {
			// Empty $_FILES array. Most likely post size was larger than post_max_size or upload_max_filesize.
			$this->response('File too big', \Response::status('Bad Request'));
		} catch (\InvalidArgumentsException $e) {
			// Some upload(validation) failed. Show errors.
			$this->response(\Upload::get_errors(), \Response::status('Bad Request'));
		} catch (\Exception $e) {
            // Something broke.
            $this->response($e, \Response::status('Bad Request'));
        }
	}
}
