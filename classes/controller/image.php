<?php
namespace Oculus;
/**
 * The Oculus Image Controller.
 *
 * @package  Oculus
 * @extends  Controller
 */
class Controller_Image extends \Controller
{
    /**
     * Get image by name.
     */
	public function action_name()
	{
		Model_Oculus::getImage(\Uri::to_assoc(3, true));
	}
	
	/**
	 * Get image by id.
	 */
	public function action_id()
	{
		Model_Oculus::getImageById(\Uri::to_assoc(3, true));
	}
}
