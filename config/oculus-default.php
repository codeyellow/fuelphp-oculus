<?php
return array(
    'path_queue' => APPPATH .  'vendor' . DS . 'codeyellow' . DS . 'fuelphp-queue' . DS,
    'path_files' => __DIR__ . DS . '..' . DS . 'files' . DS,
    'path_cache' => DOCROOT . 'oculus' . DS . 'image' . DS . 'id' . DS,
    'error_image_not_found' => __DIR__ . DS . '..' . DS . 'files' . DS . 'status' . DS . 'error' . DS . 'not_found.jpg',
    'error_invalid_argument' => __DIR__ . DS . '..' . DS . 'files' . DS . 'status' . DS . 'error' . DS . 'invalid_arguments.jpg',
    'error_unknown' => __DIR__ . DS . '..' . DS . 'files' . DS . 'status' . DS . 'error' . DS . 'unknown.jpg',
    'processing' => __DIR__ . DS . '..' . DS . 'files' . DS . 'status' . DS . 'processing.png',
    'arg_keys' => array(
        'file' => array(
            'f' 
        ), // File name
        'width' => array(
            'w' 
        ), // Width
        'height' => array(
            'h' 
        ), // Height
        'aspect_ratio' => array(
            'r' 
        ), // Height
        'padding' => array(
            'p' 
        ), // Height
        'padding_bgcolor' => array(
            'p_bg' 
        ), // Height
        'crop_resize' => array(
            'cr' 
        ),
        'quality' => array(
            'q' 
        ) 
    )
    ,
    'max_width' => 10000,
    'max_height' => 10000 
)
;

